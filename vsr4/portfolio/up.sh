#!/usr/bin/env bash

docker-compose up -d

# Dependencies
docker-compose run composer sh -c "composer install && composer dump-autoload --optimize"
docker-compose run node sh -c "yarn install && yarn run build"

# Database
docker-compose run php-fpm sh -c "wait-for-it.sh database:5432"
docker-compose run php-fpm sh -c "php ./bin/console d:d:c"
docker-compose run php-fpm sh -c "php ./bin/console d:s:c"
