# portfolio de Cyrille

## Как развернуть

Запустите `up.sh`:
```shell
sh ./up.sh
```

Сайт доступен по адресу: `http://portfolio.com`.

Так как DNS ничего не знает про это приложение, необходимо либо купить домен, либо добавить в `hosts` строку `127.0.0.1 portfolio.com`.
