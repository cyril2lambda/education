# Вариативные задания: тема №2

## Задание №2

> 2. Развёртывание интегративного решения WordPress + LifterLMS.

Необходимо подготовить два решения:

1. WordPress;
2. LifterLMS.

### Развертывание WordPress

Для развертывания WordPress необходимо:

1. Скачать исходные коды с [wodpress.org](https://wordpress.org/download/).

Так как WordPress основан на PHP, достаточно запустить серер, способный обрабатывать php-скрипты, например, Nginx + PHP-FPM, Apache или встроенный сервер PHP.

2. Подготовить MySQL базу данных (или MariaDB).

```shell
mysql -u root -e 'CREATE DATABASE wordpress'
```

![Мастер настройки WordPress](./1.PNG "Мастер настройки WordPress")

![Административная панель WordPress](./2.PNG "Административная панель WordPress")

### Интеграция LifterLMS

Для интеграции LifterLMS достаточно установить плагин.

![LifterLMS плагин](./4.PNG "LifterLMS плагин")

![LifterLMS мастер настройки](./5.PNG "LifterLMS мастер настройки")

В разделе `Плагины` добавится пункт `LifterLMS`.

В данном разделе можно создавать курсы, уроки, планы подписки на курсы и т.д.

![LifterLMS пример создания урока](./6.PNG "LifterLMS пример создания урока")
